#!/bin/bash
#cp -r fonts/ public/

#varO='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap'
  
#varN='fonts/stylesheet.css'

#sed -i -- "s|$varO|$varN|g" public/*.html

#sed -i -- "s|background-color: #fff|background-color: #232323|g" public/*.html

#sed -i -- 's|<section class="display-7".*<\/section>||g' public/*.html

var='
        <br />
        <span style="color: white;">
            <strong>Mit Hilfe dieser App kannst Du:</strong>
            <ul>
                <li>Den <strong>Vertretungsplan</strong> individuell filtern und anzeigen lassen und Du wirst &uuml;ber deine
                    Vertretungen automatisch <strong>benachrichtigt</strong>.</li>
                <li>Deinen <strong>Stundenplan</strong> digital verwalten. Außerdem kann der Plan <ul>
                        <li>Dein Handy <strong>automatisch</strong> während des Unterrichts auf <strong>stumm schalten</strong>
                        </li>
                        <li>Deine <strong>Vertretungen</strong> in deine regulären Stunden <strong>integrieren</strong></li>
                        <li>Eine <strong>Benachrichtigung</strong> mit deinem <strong>nächsten Fach</strong> senden</li>
                    </ul>
                </li>
                <li>Die <strong>Lehrerliste</strong> durchsuchen und sofort eine <strong>E-Mail</strong> an den Lehrer schreiben
                </li>
                <li>Den <strong>Raumplan</strong> durchsuchen</li>
                <li>Eine auf Smartphones sehr gut lesbare Version der <strong>Schulhomepage</strong> aufrufen</li>
                <li>Deine <strong>Noten</strong> verwalten</li>
                <li>Das <strong>Sekretariat</strong> ohne Probleme anrufen</li>
                <li><a href="https://mebis.bayern.de/"><strong>Mebis</strong></a> ohne Umwege nutzen</li>
                <li>Essen in der <a href="https://www.kitafino.de/"><strong>Mensa</strong></a> bestellen</li>
                <li><strong>Notizen</strong> anlegen und <strong>Busverbindungen</strong> suchen</li>
                <li><strong>Schülerzeitung</strong>, <strong>Schulpodcast</strong> und <strong>Schulshop</strong> abrufen</li>
            </ul>
            <p><strong>Au&szlig;erdem ist diese App auch f&uuml;r Eltern sehr n&uuml;tzlich:</strong><br />
                So kann man <strong>verschiedene Profile</strong> f&uuml;r jedes Kind anlegen und bestimmte Funktionen, die
                besonders f&uuml;r Eltern n&uuml;tzlich sind, benutzen, wie:
            </p>
            <ul>
                <li>Den <strong>Raumplan durchsuchen</strong> (besonders nützlich für Elternabende)</li>
                <li><strong>Wichtige Formulare</strong> (z.B. Krankheitsmeldungen) abrufen</li>
                <li><strong>ClaXss</strong> ohne Umwege aufrufen</li>
                <li>Ohne Probleme das <strong>Sekretariat anrufen</strong> (z.B. um ein Kind krankzumelden)</li>
                <li>Automatisch das Gymnasium in der <strong>Navigationsapp</strong> einstellen</li>
                <li>Essen f&uuml;r Ihr Kind in der <a href="https://www.kitafino.de/"><strong>Mensa</strong></a> bestellen</li>
                <li>Eine <strong>automatische Benachrichtigung</strong> &uuml;ber vertretene Stunden Ihrer Kinder</li>
            </ul>
            <p>Au&szlig;erdem kommen im Laufe der Zeit immer wieder neue Funktionen dazu.<br /> Dar&uuml;ber hinaus sind
                Vorschl&auml;ge &uuml;ber etwaige Funktionen der App immer willkommen.<br />
                Bitte schicke mir daf&uuml;r eine E-Mail an <a href="mailto:asdoi\@posteo.de">asdoi\@posteo.de</a>.
            </p>
        </span>'

var2='<p><strong>description</strong></p>'

perl -pi -e "s|$var2|$var|g" public/gymwenapp.html
